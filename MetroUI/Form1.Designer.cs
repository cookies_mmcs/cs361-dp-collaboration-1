﻿namespace Crypto
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.metroToggle1 = new MetroFramework.Controls.MetroToggle();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroToolTip1 = new MetroFramework.Components.MetroToolTip();
            this.ciphers = new System.Windows.Forms.GroupBox();
            this.metroRadioButton3 = new MetroFramework.Controls.MetroRadioButton();
            this.metroRadioButton1 = new MetroFramework.Controls.MetroRadioButton();
            this.metroRadioButton2 = new MetroFramework.Controls.MetroRadioButton();
            this.goButton = new MetroFramework.Controls.MetroButton();
            this.swapButton = new MetroFramework.Controls.MetroButton();
            this.keyBox = new MetroFramework.Controls.MetroTextBox();
            this.workersTrackBar = new MetroFramework.Controls.MetroTrackBar();
            this.countTextBox = new MetroFramework.Controls.MetroTextBox();
            this.aboutTile = new MetroFramework.Controls.MetroTile();
            this.openTile = new MetroFramework.Controls.MetroTile();
            this.saveTile = new MetroFramework.Controls.MetroTile();
            this.inputBox = new MetroFramework.Controls.MetroTextBox();
            this.outputBox = new MetroFramework.Controls.MetroTextBox();
            this.metroStyleManager1 = new MetroFramework.Components.MetroStyleManager(this.components);
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ciphers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroToggle1
            // 
            this.metroToggle1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroToggle1.AutoSize = true;
            this.metroToggle1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroToggle1.DisplayStatus = false;
            this.metroToggle1.Location = new System.Drawing.Point(594, 32);
            this.metroToggle1.Name = "metroToggle1";
            this.metroToggle1.Size = new System.Drawing.Size(50, 17);
            this.metroToggle1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroToggle1.TabIndex = 3;
            this.metroToggle1.Text = "Off";
            this.metroToggle1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroToolTip1.SetToolTip(this.metroToggle1, "Select the operating mode");
            this.metroToggle1.UseSelectable = true;
            this.metroToggle1.CheckedChanged += new System.EventHandler(this.metroToggle1_CheckedChanged);
            // 
            // metroLabel2
            // 
            this.metroLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(517, 32);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(71, 19);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel2.TabIndex = 4;
            this.metroLabel2.Text = "Encryption";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroToolTip1.SetToolTip(this.metroLabel2, "Select the operating mode");
            // 
            // metroLabel3
            // 
            this.metroLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(650, 32);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(73, 19);
            this.metroLabel3.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "Decryption";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroToolTip1.SetToolTip(this.metroLabel3, "Select the operating mode");
            // 
            // metroToolTip1
            // 
            this.metroToolTip1.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroToolTip1.StyleManager = null;
            this.metroToolTip1.Tag = "";
            this.metroToolTip1.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // ciphers
            // 
            this.ciphers.Controls.Add(this.metroRadioButton3);
            this.ciphers.Controls.Add(this.metroRadioButton1);
            this.ciphers.Controls.Add(this.metroRadioButton2);
            this.ciphers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.ciphers.ForeColor = System.Drawing.Color.SpringGreen;
            this.ciphers.Location = new System.Drawing.Point(23, 63);
            this.ciphers.Name = "ciphers";
            this.ciphers.Size = new System.Drawing.Size(185, 97);
            this.ciphers.TabIndex = 6;
            this.ciphers.TabStop = false;
            this.ciphers.Text = "Select the cipher";
            this.metroToolTip1.SetToolTip(this.ciphers, "Please, chouse chipher");
            // 
            // metroRadioButton3
            // 
            this.metroRadioButton3.AutoSize = true;
            this.metroRadioButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroRadioButton3.Location = new System.Drawing.Point(6, 64);
            this.metroRadioButton3.Name = "metroRadioButton3";
            this.metroRadioButton3.Size = new System.Drawing.Size(91, 15);
            this.metroRadioButton3.Style = MetroFramework.MetroColorStyle.Green;
            this.metroRadioButton3.TabIndex = 5;
            this.metroRadioButton3.Text = "Affine cipher";
            this.metroRadioButton3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroRadioButton3.UseSelectable = true;
            this.metroRadioButton3.CheckedChanged += new System.EventHandler(this.metroRadioButton1_CheckedChanged);
            // 
            // metroRadioButton1
            // 
            this.metroRadioButton1.AutoSize = true;
            this.metroRadioButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroRadioButton1.Location = new System.Drawing.Point(6, 22);
            this.metroRadioButton1.Name = "metroRadioButton1";
            this.metroRadioButton1.Size = new System.Drawing.Size(94, 15);
            this.metroRadioButton1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroRadioButton1.TabIndex = 5;
            this.metroRadioButton1.Text = "Caesar cipher";
            this.metroRadioButton1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroRadioButton1.UseSelectable = true;
            this.metroRadioButton1.CheckedChanged += new System.EventHandler(this.metroRadioButton1_CheckedChanged);
            // 
            // metroRadioButton2
            // 
            this.metroRadioButton2.AutoSize = true;
            this.metroRadioButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroRadioButton2.Location = new System.Drawing.Point(6, 43);
            this.metroRadioButton2.Name = "metroRadioButton2";
            this.metroRadioButton2.Size = new System.Drawing.Size(105, 15);
            this.metroRadioButton2.Style = MetroFramework.MetroColorStyle.Green;
            this.metroRadioButton2.TabIndex = 5;
            this.metroRadioButton2.Text = "Vigenère cipher";
            this.metroRadioButton2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroRadioButton2.UseSelectable = true;
            this.metroRadioButton2.CheckedChanged += new System.EventHandler(this.metroRadioButton1_CheckedChanged);
            // 
            // goButton
            // 
            this.goButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.goButton.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.goButton.Highlight = true;
            this.goButton.Location = new System.Drawing.Point(23, 381);
            this.goButton.Name = "goButton";
            this.goButton.Size = new System.Drawing.Size(175, 44);
            this.goButton.Style = MetroFramework.MetroColorStyle.Green;
            this.goButton.TabIndex = 8;
            this.goButton.Text = "Go!";
            this.goButton.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroToolTip1.SetToolTip(this.goButton, "Push to encrypt/decrypt");
            this.goButton.UseSelectable = true;
            this.goButton.Click += new System.EventHandler(this.goButton_Click);
            // 
            // swapButton
            // 
            this.swapButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("swapButton.BackgroundImage")));
            this.swapButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.swapButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.swapButton.DisplayFocus = true;
            this.swapButton.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.swapButton.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.swapButton.Highlight = true;
            this.swapButton.Location = new System.Drawing.Point(439, 28);
            this.swapButton.Name = "swapButton";
            this.swapButton.Size = new System.Drawing.Size(60, 27);
            this.swapButton.Style = MetroFramework.MetroColorStyle.Green;
            this.swapButton.TabIndex = 12;
            this.swapButton.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroToolTip1.SetToolTip(this.swapButton, "Push to copy text from output to input");
            this.swapButton.UseSelectable = true;
            this.swapButton.Click += new System.EventHandler(this.swapButton_Click);
            // 
            // keyBox
            // 
            // 
            // 
            // 
            this.keyBox.CustomButton.Image = null;
            this.keyBox.CustomButton.Location = new System.Drawing.Point(163, 1);
            this.keyBox.CustomButton.Name = "";
            this.keyBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.keyBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.keyBox.CustomButton.TabIndex = 1;
            this.keyBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.keyBox.CustomButton.UseSelectable = true;
            this.keyBox.CustomButton.Visible = false;
            this.keyBox.Lines = new string[0];
            this.keyBox.Location = new System.Drawing.Point(23, 185);
            this.keyBox.MaxLength = 32767;
            this.keyBox.Name = "keyBox";
            this.keyBox.PasswordChar = '\0';
            this.keyBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.keyBox.SelectedText = "";
            this.keyBox.SelectionLength = 0;
            this.keyBox.SelectionStart = 0;
            this.keyBox.Size = new System.Drawing.Size(185, 23);
            this.keyBox.Style = MetroFramework.MetroColorStyle.Green;
            this.keyBox.TabIndex = 13;
            this.keyBox.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroToolTip1.SetToolTip(this.keyBox, "Write the key");
            this.keyBox.UseSelectable = true;
            this.keyBox.WaterMark = "Key...";
            this.keyBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.keyBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.keyBox.TextChanged += new System.EventHandler(this.keyBox_TextChanged);
            this.keyBox.Click += new System.EventHandler(this.keyBox_Click);
            this.keyBox.Leave += new System.EventHandler(this.keyBox_Leave);
            // 
            // workersTrackBar
            // 
            this.workersTrackBar.BackColor = System.Drawing.Color.Transparent;
            this.workersTrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.workersTrackBar.Location = new System.Drawing.Point(69, 233);
            this.workersTrackBar.Minimum = 1;
            this.workersTrackBar.Name = "workersTrackBar";
            this.workersTrackBar.Size = new System.Drawing.Size(139, 25);
            this.workersTrackBar.Style = MetroFramework.MetroColorStyle.Green;
            this.workersTrackBar.TabIndex = 15;
            this.workersTrackBar.Text = "metroTrackBar1";
            this.workersTrackBar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroToolTip1.SetToolTip(this.workersTrackBar, "Select count of workers");
            this.workersTrackBar.Value = 10;
            this.workersTrackBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.workersTrackBar_Scroll);
            // 
            // countTextBox
            // 
            // 
            // 
            // 
            this.countTextBox.CustomButton.Image = null;
            this.countTextBox.CustomButton.Location = new System.Drawing.Point(18, 1);
            this.countTextBox.CustomButton.Name = "";
            this.countTextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.countTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.countTextBox.CustomButton.TabIndex = 1;
            this.countTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.countTextBox.CustomButton.UseSelectable = true;
            this.countTextBox.CustomButton.Visible = false;
            this.countTextBox.Lines = new string[] {
        "10"};
            this.countTextBox.Location = new System.Drawing.Point(23, 234);
            this.countTextBox.MaxLength = 32767;
            this.countTextBox.Name = "countTextBox";
            this.countTextBox.PasswordChar = '\0';
            this.countTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.countTextBox.SelectedText = "";
            this.countTextBox.SelectionLength = 0;
            this.countTextBox.SelectionStart = 0;
            this.countTextBox.Size = new System.Drawing.Size(40, 23);
            this.countTextBox.Style = MetroFramework.MetroColorStyle.Green;
            this.countTextBox.TabIndex = 17;
            this.countTextBox.Text = "10";
            this.countTextBox.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroToolTip1.SetToolTip(this.countTextBox, "Select count of workers");
            this.countTextBox.UseSelectable = true;
            this.countTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.countTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.countTextBox.TextChanged += new System.EventHandler(this.countTextBox_TextChanged);
            this.countTextBox.Leave += new System.EventHandler(this.metroTextBox1_Leave);
            // 
            // aboutTile
            // 
            this.aboutTile.ActiveControl = null;
            this.aboutTile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.aboutTile.Location = new System.Drawing.Point(260, 28);
            this.aboutTile.Name = "aboutTile";
            this.aboutTile.Size = new System.Drawing.Size(142, 27);
            this.aboutTile.Style = MetroFramework.MetroColorStyle.Green;
            this.aboutTile.TabIndex = 18;
            this.aboutTile.Text = "About";
            this.aboutTile.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroToolTip1.SetToolTip(this.aboutTile, "Push to see information about programm and developers");
            this.aboutTile.UseSelectable = true;
            this.aboutTile.Click += new System.EventHandler(this.metroTile1_Click);
            // 
            // openTile
            // 
            this.openTile.ActiveControl = null;
            this.openTile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.openTile.Location = new System.Drawing.Point(23, 284);
            this.openTile.Name = "openTile";
            this.openTile.Size = new System.Drawing.Size(76, 73);
            this.openTile.Style = MetroFramework.MetroColorStyle.Green;
            this.openTile.TabIndex = 19;
            this.openTile.Text = "Open file";
            this.openTile.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroToolTip1.SetToolTip(this.openTile, "Push to open txt file");
            this.openTile.UseSelectable = true;
            this.openTile.Click += new System.EventHandler(this.openTile_Click);
            // 
            // saveTile
            // 
            this.saveTile.ActiveControl = null;
            this.saveTile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.saveTile.Location = new System.Drawing.Point(122, 284);
            this.saveTile.Name = "saveTile";
            this.saveTile.Size = new System.Drawing.Size(76, 73);
            this.saveTile.Style = MetroFramework.MetroColorStyle.Green;
            this.saveTile.TabIndex = 19;
            this.saveTile.Text = "Save file";
            this.saveTile.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroToolTip1.SetToolTip(this.saveTile, "Click to save file");
            this.saveTile.UseSelectable = true;
            this.saveTile.Click += new System.EventHandler(this.saveTile_Click);
            // 
            // inputBox
            // 
            this.inputBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.inputBox.CustomButton.Image = null;
            this.inputBox.CustomButton.Location = new System.Drawing.Point(-112, 1);
            this.inputBox.CustomButton.Name = "";
            this.inputBox.CustomButton.Size = new System.Drawing.Size(351, 351);
            this.inputBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.inputBox.CustomButton.TabIndex = 1;
            this.inputBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.inputBox.CustomButton.UseSelectable = true;
            this.inputBox.CustomButton.Visible = false;
            this.inputBox.Lines = new string[0];
            this.inputBox.Location = new System.Drawing.Point(3, 3);
            this.inputBox.MaxLength = 32767;
            this.inputBox.Multiline = true;
            this.inputBox.Name = "inputBox";
            this.inputBox.PasswordChar = '\0';
            this.inputBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.inputBox.SelectedText = "";
            this.inputBox.SelectionLength = 0;
            this.inputBox.SelectionStart = 0;
            this.inputBox.Size = new System.Drawing.Size(240, 353);
            this.inputBox.Style = MetroFramework.MetroColorStyle.Green;
            this.inputBox.TabIndex = 7;
            this.inputBox.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.inputBox.UseSelectable = true;
            this.inputBox.WaterMark = "Input text";
            this.inputBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.inputBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.inputBox.Click += new System.EventHandler(this.inputBox_Click);
            this.inputBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.inputBox_DragDrop);
            this.inputBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.inputBox_DragEnter);
            this.inputBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inputBox_KeyDown);
            this.inputBox.Leave += new System.EventHandler(this.inputBox_Leave);
            // 
            // outputBox
            // 
            this.outputBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.outputBox.CustomButton.Image = null;
            this.outputBox.CustomButton.Location = new System.Drawing.Point(-111, 2);
            this.outputBox.CustomButton.Name = "";
            this.outputBox.CustomButton.Size = new System.Drawing.Size(347, 347);
            this.outputBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.outputBox.CustomButton.TabIndex = 1;
            this.outputBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.outputBox.CustomButton.UseSelectable = true;
            this.outputBox.CustomButton.Visible = false;
            this.outputBox.Lines = new string[0];
            this.outputBox.Location = new System.Drawing.Point(3, 3);
            this.outputBox.MaxLength = 32767;
            this.outputBox.Multiline = true;
            this.outputBox.Name = "outputBox";
            this.outputBox.PasswordChar = '\0';
            this.outputBox.ReadOnly = true;
            this.outputBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.outputBox.SelectedText = "";
            this.outputBox.SelectionLength = 0;
            this.outputBox.SelectionStart = 0;
            this.outputBox.Size = new System.Drawing.Size(239, 352);
            this.outputBox.Style = MetroFramework.MetroColorStyle.Green;
            this.outputBox.TabIndex = 7;
            this.outputBox.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.outputBox.UseSelectable = true;
            this.outputBox.WaterMark = "Output text";
            this.outputBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.outputBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.outputBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inputBox_KeyDown);
            // 
            // metroStyleManager1
            // 
            this.metroStyleManager1.Owner = null;
            this.metroStyleManager1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroStyleManager1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(29, 163);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(125, 19);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel1.TabIndex = 14;
            this.metroLabel1.Text = "Write down the key:";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(23, 211);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(186, 19);
            this.metroLabel4.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel4.TabIndex = 16;
            this.metroLabel4.Text = "Select count of workers (1..100)";
            this.metroLabel4.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Text files (*.txt)|*.txt";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Text files (*.txt)|*.txt";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(23, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(202, 40);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BackColor = System.Drawing.Color.Black;
            this.splitContainer1.ForeColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Location = new System.Drawing.Point(228, 69);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.inputBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.outputBox);
            this.splitContainer1.Size = new System.Drawing.Size(495, 362);
            this.splitContainer1.SplitterDistance = 246;
            this.splitContainer1.TabIndex = 21;
            // 
            // MainForm
            // 
            this.ApplyImageInvert = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(746, 454);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.saveTile);
            this.Controls.Add(this.openTile);
            this.Controls.Add(this.aboutTile);
            this.Controls.Add(this.countTextBox);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.workersTrackBar);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.keyBox);
            this.Controls.Add(this.swapButton);
            this.Controls.Add(this.goButton);
            this.Controls.Add(this.ciphers);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroToggle1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(746, 454);
            this.Name = "MainForm";
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ciphers.ResumeLayout(false);
            this.ciphers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroToggle metroToggle1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Components.MetroToolTip metroToolTip1;
        private MetroFramework.Controls.MetroRadioButton metroRadioButton1;
        private MetroFramework.Controls.MetroRadioButton metroRadioButton2;
        private MetroFramework.Controls.MetroRadioButton metroRadioButton3;
        private System.Windows.Forms.GroupBox ciphers;
        private MetroFramework.Controls.MetroTextBox inputBox;
        private MetroFramework.Controls.MetroTextBox outputBox;
        private MetroFramework.Controls.MetroButton goButton;
        private MetroFramework.Components.MetroStyleManager metroStyleManager1;
        private MetroFramework.Controls.MetroButton swapButton;
        private MetroFramework.Controls.MetroTextBox keyBox;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTrackBar workersTrackBar;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox countTextBox;
        private MetroFramework.Controls.MetroTile aboutTile;
        private MetroFramework.Controls.MetroTile openTile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private MetroFramework.Controls.MetroTile saveTile;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}

