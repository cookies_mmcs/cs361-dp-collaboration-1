﻿namespace Crypto
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
            this.bucketTile = new MetroFramework.Controls.MetroTile();
            this.quitTile = new MetroFramework.Controls.MetroTile();
            this.rateTile = new MetroFramework.Controls.MetroTile();
            this.metroTile1 = new MetroFramework.Controls.MetroTile();
            this.metroTile2 = new MetroFramework.Controls.MetroTile();
            this.metroTile3 = new MetroFramework.Controls.MetroTile();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // bucketTile
            // 
            this.bucketTile.ActiveControl = null;
            this.bucketTile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bucketTile.Location = new System.Drawing.Point(23, 478);
            this.bucketTile.Name = "bucketTile";
            this.bucketTile.Size = new System.Drawing.Size(88, 74);
            this.bucketTile.Style = MetroFramework.MetroColorStyle.Blue;
            this.bucketTile.TabIndex = 0;
            this.bucketTile.Text = "BitBucket";
            this.bucketTile.UseSelectable = true;
            this.bucketTile.Click += new System.EventHandler(this.bucketTile_Click);
            // 
            // quitTile
            // 
            this.quitTile.ActiveControl = null;
            this.quitTile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.quitTile.Location = new System.Drawing.Point(117, 478);
            this.quitTile.Name = "quitTile";
            this.quitTile.Size = new System.Drawing.Size(88, 74);
            this.quitTile.Style = MetroFramework.MetroColorStyle.Orange;
            this.quitTile.TabIndex = 0;
            this.quitTile.Text = "Close";
            this.quitTile.UseSelectable = true;
            this.quitTile.Click += new System.EventHandler(this.quitTile_Click);
            // 
            // rateTile
            // 
            this.rateTile.ActiveControl = null;
            this.rateTile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rateTile.Location = new System.Drawing.Point(23, 391);
            this.rateTile.Name = "rateTile";
            this.rateTile.Size = new System.Drawing.Size(182, 81);
            this.rateTile.Style = MetroFramework.MetroColorStyle.Green;
            this.rateTile.TabIndex = 1;
            this.rateTile.Text = "Rate job";
            this.rateTile.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.rateTile.UseSelectable = true;
            this.rateTile.UseTileImage = true;
            this.rateTile.Click += new System.EventHandler(this.rateTile_Click);
            // 
            // metroTile1
            // 
            this.metroTile1.ActiveControl = null;
            this.metroTile1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroTile1.Location = new System.Drawing.Point(23, 311);
            this.metroTile1.Name = "metroTile1";
            this.metroTile1.Size = new System.Drawing.Size(113, 74);
            this.metroTile1.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroTile1.TabIndex = 0;
            this.metroTile1.Text = "Affine cipher";
            this.metroTile1.UseSelectable = true;
            this.metroTile1.Click += new System.EventHandler(this.metroTile1_Click);
            // 
            // metroTile2
            // 
            this.metroTile2.ActiveControl = null;
            this.metroTile2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroTile2.Location = new System.Drawing.Point(23, 231);
            this.metroTile2.Name = "metroTile2";
            this.metroTile2.Size = new System.Drawing.Size(113, 74);
            this.metroTile2.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroTile2.TabIndex = 0;
            this.metroTile2.Text = "Vigenère cipher";
            this.metroTile2.UseSelectable = true;
            this.metroTile2.Click += new System.EventHandler(this.metroTile2_Click);
            // 
            // metroTile3
            // 
            this.metroTile3.ActiveControl = null;
            this.metroTile3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroTile3.Location = new System.Drawing.Point(23, 151);
            this.metroTile3.Name = "metroTile3";
            this.metroTile3.Size = new System.Drawing.Size(113, 74);
            this.metroTile3.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroTile3.TabIndex = 0;
            this.metroTile3.Text = "Caesar cipher";
            this.metroTile3.UseSelectable = true;
            this.metroTile3.Click += new System.EventHandler(this.metroTile3_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(210, 112);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(311, 133);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "DEVELOPERS:\r\n\r\nAlexandra AWESOME - testing and Vigenère cipher\r\n\r\nDmitriy THE BES" +
    "T - back end and Caesar cipher\r\n\r\nArtemiy FABULOUS - front end and Affine cipher" +
    "";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(258, 311);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(254, 224);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // About
            // 
            this.ApplyImageInvert = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(553, 575);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.rateTile);
            this.Controls.Add(this.quitTile);
            this.Controls.Add(this.metroTile3);
            this.Controls.Add(this.metroTile2);
            this.Controls.Add(this.metroTile1);
            this.Controls.Add(this.bucketTile);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "About";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.Style = MetroFramework.MetroColorStyle.Magenta;
            this.Text = "About";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.About_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTile bucketTile;
        private MetroFramework.Controls.MetroTile quitTile;
        private MetroFramework.Controls.MetroTile rateTile;
        private MetroFramework.Controls.MetroTile metroTile1;
        private MetroFramework.Controls.MetroTile metroTile2;
        private MetroFramework.Controls.MetroTile metroTile3;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}