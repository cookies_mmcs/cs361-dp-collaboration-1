﻿using MetroFramework.Forms;
using Encryption_Module;
using Encryption_Module.Factories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;

namespace Crypto
{
    public partial class MainForm : MetroForm
    {
        public MainForm()
        {
            InitializeComponent();
            countTextBox.Text = workersTrackBar.Value.ToString();
            metroRadioButton1.Checked = true;
            keyboxerr();
            inputBox.AllowDrop = true;
            inputBox.DragEnter += new DragEventHandler(inputBox_DragEnter);
            inputBox.DragDrop += new DragEventHandler(inputBox_DragDrop);
        }

        private void inputBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop) &&
                ((e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move))

                e.Effect = DragDropEffects.Move;
        }

        private void inputBox_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop) && e.Effect == DragDropEffects.Move)
            {
                string[] objects = (string[])e.Data.GetData(DataFormats.FileDrop);
                if (objects[0].Contains("txt"))
                {
                    StreamReader sr = new StreamReader(objects[0]);
                    inputBox.Text = sr.ReadToEnd() + "\r\n";
                    sr.Close();
                }
            }
        }

        private void inputBox_Click(object sender, EventArgs e)
        {
            this.inputBox.WaterMark = "";
        }

        private void inputBox_Leave(object sender, EventArgs e)
        {
            inputBox.WaterMark = "Input text";
        }

        private void keyBox_Click(object sender, EventArgs e)
        {
            keyBox.WaterMark = "";
            keyBox.Style = MetroFramework.MetroColorStyle.Red;
        }

        private void keyBox_Leave(object sender, EventArgs e)
        {
            keyBox.WaterMark = "Key...";
            if (keyBox.Text.Length == 0)
            {
                keyboxerr();
            }
        }

        private void metroTextBox1_Leave(object sender, EventArgs e)
        {
            int t;
            bool f = int.TryParse(countTextBox.Text, out t);
            if (t > workersTrackBar.Maximum)
                countTextBox.Text = "100";
            if (t <= 0)
                countTextBox.Text = "1";
            if ((countTextBox.Text.Length <= 0) || !f)
            {
                countTextBox.Text = "10";
            }
        }

        private void workersTrackBar_Scroll(object sender, ScrollEventArgs e)
        {
            countTextBox.Text = workersTrackBar.Value.ToString();
        }

        private void countTextBox_TextChanged(object sender, EventArgs e)
        {
            int t;
            bool f = int.TryParse(countTextBox.Text, out t);
            if (f && (t <= workersTrackBar.Maximum) && (t > workersTrackBar.Minimum))
            {
                countTextBox.Style = MetroFramework.MetroColorStyle.Green;
                countTextBox.ForeColor = Color.Green;
                workersTrackBar.Value = t;
            }
            else
            {
                countTextBox.Style = MetroFramework.MetroColorStyle.Red;
                countTextBox.ForeColor = Color.Red;
            }


        }

        bool isLatin(string s)
        {
            bool f = true;
            for (int i = 0; i < s.Length; i++)
            {
                char c = s[i];
                if (!(c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z'))
                    f = false;
            }
            return f;
        }

        void keyboxerr()
        {
            keyBox.Style = MetroFramework.MetroColorStyle.Red;
            keyBox.ForeColor = Color.Red;
            goButton.Style = MetroFramework.MetroColorStyle.Red;
            goButton.Enabled = false;
        }

        void keyboxok()
        {
            keyBox.Style = MetroFramework.MetroColorStyle.Green;
            keyBox.ForeColor = Color.Green;
            goButton.Style = MetroFramework.MetroColorStyle.Green;
            goButton.Enabled = true;
        }

        private void keyBox_TextChanged(object sender, EventArgs e)
        {
            int t;
            bool f = int.TryParse(keyBox.Text, out t);
            if (metroRadioButton1.Checked || metroRadioButton3.Checked)
            {
                if (!f)
                {
                    keyboxerr();
                }
                else
                {
                    keyboxok();
                }
            }
            else
            {
                if (metroRadioButton2.Checked)
                {
                    if (!isLatin(keyBox.Text) || (keyBox.Text.Length == 0))
                    {
                        keyboxerr();
                    }
                    else
                    {
                        keyboxok();
                    }
                }
                else
                {
                    keyboxerr();
                }
            }
        }

        private void metroRadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            keyBox.Text = "";
            keyBox.Focus();
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            About ab = new About();
            ab.ShowDialog();
        }

        private void swapButton_Click(object sender, EventArgs e)
        {
            if (outputBox.Text.Length != 0)
            {
                inputBox.Text = outputBox.Text;
                outputBox.Text = "";
                if (metroToggle1.CheckState == CheckState.Checked)
                {
                    metroToggle1.CheckState = CheckState.Unchecked;
                }
                else
                {
                    metroToggle1.CheckState = CheckState.Checked;
                }
            }
        }

        private void openTile_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(openFileDialog1.FileName);
                inputBox.Text = sr.ReadToEnd();
            }
            
        }

        private void saveTile_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text document (*.txt)|*.txt";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter streamWriter = new StreamWriter(saveFileDialog.FileName);
                streamWriter.WriteLine(outputBox.Text);
                streamWriter.Close();
            }
        }

        private void inputBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.A)
            {
                inputBox.SelectAll();
            }
        }

        private void goButton_Click(object sender, EventArgs e)
        {
            outputBox.Focus();
            int c;
            Superintendent si = new Superintendent();
            if (metroRadioButton1.Checked)
            {
                c = 0;
            }
            else
            {
                if (metroRadioButton2.Checked)
                {
                    c = 1;
                }
                else
                    c = 2;
            }
            si.Init(!metroToggle1.Checked, inputBox.Text, workersTrackBar.Value, keyBox.Text, c);
            outputBox.Text = si.WorkWithCipher();
        }

        private void metroToggle1_CheckedChanged(object sender, EventArgs e)
        {
            outputBox.Text = "";
        }
    }
}
