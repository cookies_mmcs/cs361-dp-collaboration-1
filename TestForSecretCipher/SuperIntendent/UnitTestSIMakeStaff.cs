﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Encryption_Module;
using Encryption_Module.Factories;


namespace TestForSecretCipher.SuperIntendent
{
    [TestClass]
    public class UnitTestSIMakeStaff
    {
        Superintendent si = new Superintendent();

        [TestMethod]
        public void TestMethod1()
        {
            si.Init(true, "abcd", 2, "10", 0);
            List<Caesar_Worker> tmp = new List<Caesar_Worker>();
            List<AbstractWorker> ms = si.MakeStaff(new Caesar_Factory(), si.SpreadingWork());
            Assert.AreEqual(ms.Count, 2);
            for (int i = 0; i < ms.Count; ++i)
            {
                tmp.Add(new Caesar_Worker());
                tmp[i].Init(10);
                int t = (ms[i] as Caesar_Worker).shift;
                Assert.AreEqual(t, tmp[i].shift);
            }
        }

        [TestMethod]
        public void TestMethod2()
        {
            si.Init(true, "abcd", 10, "-101", 2);
            List<AbstractWorker> ms = si.MakeStaff(new Caesar_Factory(), si.SpreadingWork());
            Assert.AreEqual(ms.Count, 10);
            List<Affine_Worker> tmp = new List<Affine_Worker>();
            for (int i = 0; i < ms.Count; ++i)
            {
                tmp.Add(new Affine_Worker());
                tmp[i].Init(-101);
                Assert.AreEqual((ms[i] as Affine_Worker).b, tmp[i].b);
            }
        }

        [TestMethod]
        public void TestMethod3()
        {
            si.Init(true, "abcdefg", 3, "abcd", 1);
            List<Vigenere_Worker> tmp = new List<Vigenere_Worker>();
            List<AbstractWorker> ms = si.MakeStaff(new Vigenere_Factory(), si.SpreadingWork());
            Assert.AreEqual(ms.Count, 3);
            tmp.Add(new Vigenere_Worker());
            tmp[0].Init(0, "abcdefg");
            tmp.Add(new Vigenere_Worker());
            tmp[1].Init(3, "abcdefg");
            tmp.Add(new Vigenere_Worker());
            tmp[2].Init(5, "abcdefg");
            
            Vigenere_Worker vw = ms[0] as Vigenere_Worker;
            //Assert.AreEqual(vw.pos, 0);
            List<int> tmp1 = new List<int> { 0, 1, 2, 3};
            for (int i = 0; i < vw.key_shift.Count; ++i) ;
                //Assert.AreEqual(vw.key_shift[i], tmp1[i]);

            vw = ms[1] as Vigenere_Worker;
            //Assert.AreEqual(vw.pos, 3);
            tmp1 = new List<int> { 3, 0, 1, 2 };
            for (int i = 0; i < vw.key_shift.Count; ++i) ;
                //Assert.AreEqual(vw.key_shift[i], tmp1[i]);

            vw = ms[2] as Vigenere_Worker;
            //Assert.AreEqual(vw.pos, 5);
            tmp1 = new List<int> { 1, 2, 3, 0 };
            for (int i = 0; i < vw.key_shift.Count; ++i) ;
                //Assert.AreEqual(vw.key_shift[i], tmp1[i]);
        }
    }
}
