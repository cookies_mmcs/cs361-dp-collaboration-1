﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Encryption_Module;

namespace TestForSecretCipher.SuperIntendent
{
    [TestClass]
    public class UnitTestSIWorkWithCipher
    {
        Superintendent si = new Superintendent();

        [TestMethod]
        public void TestMethod1()
        {
            si.Init(true, "!abcd!", 100, "-10", 0);
            Assert.AreEqual(si.WorkWithCipher(), "!qrst!");
        }

        [TestMethod]
        public void TestMethod2()
        {
            si.Init(false, "!qrst!", 100, "-10", 0);
            Assert.AreEqual(si.WorkWithCipher(), "!abcd!");
        }

        [TestMethod]
        public void TestMethod3()
        {
            si.Init(true, "!abcd!", 1, "abcd", 1);
            string tmp = si.WorkWithCipher();
            Assert.AreEqual(si.WorkWithCipher(), "!bdfd!");
        }

        [TestMethod]
        public void TestMethod4()
        {
            si.Init(false, "!bdfd!", 9, "abcd", 1);
            Assert.AreEqual(si.WorkWithCipher(), "!abcd!");
        }

        [TestMethod]
        public void TestMethod5()
        {
            si.Init(false, "!abcD!", 100, "-10", 2);
            Assert.AreEqual(si.WorkWithCipher(), "!qpoN!");
        }

        [TestMethod]
        public void TestMethod6()
        {
            si.Init(false, "!qpoN!", 1, "-10", 2);
            Assert.AreEqual(si.WorkWithCipher(), "!abcD!");
        }
    }
}
