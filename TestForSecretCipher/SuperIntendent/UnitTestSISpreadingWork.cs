﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Encryption_Module;


namespace TestForSecretCipher.SuperIntendent
{
    [TestClass]
    public class UnitTestSISpreadingWork
    {
        Superintendent si = new Superintendent();

        [TestMethod]
        public void TestMethod1()
        {
            si.Init(true, "abcd", 4, "1", 0);
            int[] tmp = new int[] {1, 1, 1, 1};
            for (int i = 0; i < tmp.Length; ++i)
                Assert.AreEqual(tmp[i], (si.SpreadingWork())[i]);
        }

        [TestMethod]
        public void TestMethod2()
        {
            si.Init(true, "abcde", 4, "1", 0);
            int[] tmp = new int[] { 2, 1, 1, 1 };
            for (int i = 0; i < tmp.Length; ++i)
                Assert.AreEqual(tmp[i], (si.SpreadingWork())[i]);
        }

        [TestMethod]
        public void TestMethod3()
        {
            si.Init(true, "abcd", 5, "1", 0);
            int[] tmp = new int[] { 1, 1, 1, 1, 0 };
            for (int i = 0; i < tmp.Length; ++i)
                Assert.AreEqual(tmp[i], (si.SpreadingWork())[i]);
        }

        [TestMethod]
        public void TestMethod4()
        {
            si.Init(true, "abcd", 1, "1", 0);
            int[] tmp = new int[] { 4 };
            for (int i = 0; i < tmp.Length; ++i)
                Assert.AreEqual(tmp[i], (si.SpreadingWork())[i]);
        }

        [TestMethod]
        public void TestMethod5()
        {
            si.Init(true, "abcdefg", 2, "1", 0);
            int[] tmp = new int[] { 4, 3 };
            for (int i = 0; i < tmp.Length; ++i)
                Assert.AreEqual(tmp[i], (si.SpreadingWork())[i]);
        }

        [TestMethod]
        public void TestMethod6()
        {
            si.Init(true, "abcd", 4, "1", 2);
            int[] tmp = new int[] { 1, 1, 1, 1 };
            for (int i = 0; i < tmp.Length; ++i)
                Assert.AreEqual(tmp[i], (si.SpreadingWork())[i]);
        }

        [TestMethod]
        public void TestMethod7()
        {
            si.Init(true, "abcde", 4, "1", 2);
            int[] tmp = new int[] { 2, 1, 1, 1 };
            for (int i = 0; i < tmp.Length; ++i)
                Assert.AreEqual(tmp[i], (si.SpreadingWork())[i]);
        }

        [TestMethod]
        public void TestMethod8()
        {
            si.Init(true, "abcd", 5, "1", 2);
            int[] tmp = new int[] { 1, 1, 1, 1, 0 };
            for (int i = 0; i < tmp.Length; ++i)
                Assert.AreEqual(tmp[i], (si.SpreadingWork())[i]);
        }

        [TestMethod]
        public void TestMethod9()
        {
            si.Init(true, "abcd", 1, "1", 2);
            int[] tmp = new int[] { 4 };
            for (int i = 0; i < tmp.Length; ++i)
                Assert.AreEqual(tmp[i], (si.SpreadingWork())[i]);
        }

        [TestMethod]
        public void TestMethod10()
        {
            si.Init(true, "abcdefg", 2, "1", 2);
            int[] tmp = new int[] { 4, 3 };
            for (int i = 0; i < tmp.Length; ++i)
                Assert.AreEqual(tmp[i], (si.SpreadingWork())[i]);
        }


    }
}
