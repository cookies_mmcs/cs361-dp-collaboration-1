﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Encryption_Module;

namespace TestForSecretCipher
{
    [TestClass]
    public class UnitTestSuperintendentInit
    {
        Superintendent si = new Superintendent();

        [TestMethod]
        public void TestMethodInit1()
        {
            si.Init(true, "abcd", 1, "1", 0);
            Assert.AreEqual(si.crypt_direction, true);
            Assert.AreEqual(si.text, "abcd");
            Assert.AreEqual(si.workers, 1);
            Assert.AreEqual(si.key, "1");
            Assert.AreEqual(si.cipher, 0);
        }

        [TestMethod]
        public void TestMethodInit2()
        {
            si.Init(true, "abcd", 1, "aaaa", 1);
            Assert.AreEqual(si.crypt_direction, true);
            Assert.AreEqual(si.text, "abcd");
            Assert.AreEqual(si.workers, 1);
            Assert.AreEqual(si.key, "aaaa");
            Assert.AreEqual(si.cipher, 1);
        }

        [TestMethod]
        public void TestMethodInit3()
        {
            si.Init(true, "abcd", 1, "25", 2);
            Assert.AreEqual(si.crypt_direction, true);
            Assert.AreEqual(si.text, "abcd");
            Assert.AreEqual(si.workers, 1);
            Assert.AreEqual(si.key, "25");
            Assert.AreEqual(si.cipher, 2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit4()
        {
            si.Init(true, "abcd", 0, "25", 2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit5()
        {
            si.Init(true, "abcd", -8, "25", 2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit6()
        {
            si.Init(true, "abcd", 100, "25", -1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit7()
        {
            si.Init(true, "abcd", 10, "25", 3);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit8()
        {
            si.Init(true, "abcd", 10, "abs", 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit9()
        {
            si.Init(true, "abcd", 10, "25 1", 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit10()
        {
            si.Init(true, "abcd", 10, "25.8", 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit11()
        {
            si.Init(true, "abcd", 10, "(-25)", 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit12()
        {
            si.Init(true, "abcd", 10, "1.0", 2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit13()
        {
            si.Init(true, "abcd", 10, "-9.25", 2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit14()
        {
            si.Init(true, "abcd", 10, "as", 2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit15()
        {
            si.Init(true, "abcd", 10, "ab as", 1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit16()
        {
            si.Init(true, "abcd", 10, "1234", 1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit17()
        {
            si.Init(true, "abcd", 10, "2, 6", 1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit18()
        {
            si.Init(true, "abcd", 10, "!#", 1);
        }

    }
}
