﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Encryption_Module;
using Encryption_Module.Factories;

namespace TestForSecretCipher.SuperIntendent
{
    [TestClass]
    public class UnitTestSIResultText
    {
        Superintendent si = new Superintendent();

        [TestMethod]
        public void TestMethod1()
        {
            si.Init(true, "abcd", 1, "1", 0);
            Assert.AreEqual(si.ResultText(
                si.MakeStaff(new Caesar_Factory(), si.SpreadingWork()), si.SpreadingWork()), "bcde");
        }

        [TestMethod]
        public void TestMethod2()
        {
            si.Init(true, "abcd", 1, "-1", 0);
            Assert.AreEqual(si.ResultText(
                si.MakeStaff(new Caesar_Factory(), si.SpreadingWork()), si.SpreadingWork()), "zabc");
        }

        [TestMethod]
        public void TestMethod3()
        {
            si.Init(false, "Abcd", 100, "ca", 1);
            Assert.AreEqual(si.ResultText(
                si.MakeStaff(new Vigenere_Factory(), si.SpreadingWork()), si.SpreadingWork()), "Ybad");
        }

        [TestMethod]
        public void TestMethod4()
        {
            si.Init(false, "o", 10, "-110", 2);
            Assert.AreEqual(si.ResultText(
                si.MakeStaff(new Affine_Factory(), si.SpreadingWork()), si.SpreadingWork()), "g");
        }
    }
}
