﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Encryption_Module;

namespace TestForSecretCipher.Vigenere
{
    [TestClass]
    public class UnitTestVigenereInit
    {
        Vigenere_Worker vw = new Vigenere_Worker();

        [TestMethod]
        public void TestMethodInit1()
        {
            vw.Init(0, "abcdef");
            Assert.AreEqual(vw.pos, 0);
            List<int> tmp = new List<int> { 0, 1, 2, 3, 4, 5 };
            for (int i = 0; i < vw.key_shift.Count; ++i)
                Assert.AreEqual(vw.key_shift[i], tmp[i]);
        }

        [TestMethod]
        public void TestMethodInit2()
        {
            vw.Init(3, "xyzdef");
            Assert.AreEqual(vw.pos, 3);
            List<int> tmp = new List<int> { 3, 4, 5, 23, 24, 25 };
            for (int i = 0; i < vw.key_shift.Count; ++i)
                Assert.AreEqual(vw.key_shift[i], tmp[i]);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit3()
        {
            vw.Init(3, "!xyz");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit4()
        {
            vw.Init(-8, "xyz");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethodInit5()
        {
            vw.Init(9, "");
        }

    }
}
