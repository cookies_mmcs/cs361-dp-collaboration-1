﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Encryption_Module;

namespace TestForSecretCipher.Vigenere
{
    [TestClass]
    public class UnitTestVigenereEncrypt
    {

        Vigenere_Worker vw = new Vigenere_Worker();

        [TestMethod]
        public void TestMethodEncrypt1()
        {
            vw.Init(0, "lemon");
            Assert.AreEqual(vw.Encrypt("abcdef"), "lforrq");
        }


        [TestMethod]
        public void TestMethodEncrypt2()
        {
            vw.Init(8, "LeMoN");
            Assert.AreEqual(vw.Encrypt("AbcdeF"), "OonhqT");
        }

        [TestMethod]
        public void TestMethodEncrypt3()
        {
            vw.Init(10, "A");
            Assert.AreEqual(vw.Encrypt("abcdef"), "abcdef");
        }

        [TestMethod]
        public void TestMethodEncrypt4()
        {
            vw.Init(15, "llleeemmmooonnn");
            Assert.AreEqual(vw.Encrypt("ABCDEfghijK"), "LMNHIjstuxY");
        }

        [TestMethod]
        public void TestMethodEncrypt5()
        {
            vw.Init(20, "lemon");
            Assert.AreEqual(vw.Encrypt("ATTACK AT DAWN"), "LXFOPV MH OEIB");
        }

        [TestMethod]
        public void TestMethodEncrypt6()
        {
            vw.Init(0, "lemon");
            Assert.AreEqual(vw.Encrypt("a!b!c!d!"), "l!n!p!h!");
        }

        [TestMethod]
        public void TestMethodEncrypt7()
        {
            vw.Init(7, "a");
            Assert.AreEqual(vw.Encrypt("!#$%567:78*^.,/'}{[]<>()"), "!#$%567:78*^.,/'}{[]<>()");
        }

        [TestMethod]
        public void TestMethodEncrypt8()
        {
            vw.Init(0, "lemon");
            Assert.AreEqual(vw.Encrypt("abcdef"), "lforrq");
        }

        [TestMethod]
        public void TestMethodEncrypt9()
        {
            vw.Init(12, "z");
            Assert.AreEqual(vw.Encrypt("XYZHI"), "WXYGH");
        }

        [TestMethod]
        public void TestMethodEncrypt10()
        {
            vw.Init(0, "a");
            Assert.AreEqual(vw.Encrypt(""), "");
        }

        [TestMethod]
        public void TestMethodEncrypt11()
        {
            vw.Init(0, "abcd");
            Assert.AreEqual(vw.Encrypt("abcd"), "aceg");
        }

    }
}
