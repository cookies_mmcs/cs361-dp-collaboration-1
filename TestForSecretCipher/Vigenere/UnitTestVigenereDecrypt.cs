﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Encryption_Module;

namespace TestForSecretCipher.Vigenere
{
    [TestClass]
    public class UnitTestVigenereDecrypt
    {

        Vigenere_Worker vw = new Vigenere_Worker();

        [TestMethod]
        public void TestMethodDecrypt1()
        {
            vw.Init(0, "lemon");
            Assert.AreEqual(vw.Decrypt("lforrq"), "abcdef");
        }


        [TestMethod]
        public void TestMethodDecrypt2()
        {
            vw.Init(8, "LeMoN");
            Assert.AreEqual(vw.Decrypt("OonhqT"), "AbcdeF");
        }

        [TestMethod]
        public void TestMethodDecrypt3()
        {
            vw.Init(10, "A");
            Assert.AreEqual(vw.Decrypt("abcdef"), "abcdef");
        }

        [TestMethod]
        public void TestMethodDecrypt4()
        {
            vw.Init(15, "llleeemmmooonnn");
            Assert.AreEqual(vw.Decrypt("LMNHIjstuxY"), "ABCDEfghijK");
        }

        [TestMethod]
        public void TestMethodDecrypt5()
        {
            vw.Init(20, "lemon");
            Assert.AreEqual(vw.Decrypt("LXFOPV MH OEIB"), "ATTACK AT DAWN");
        }

        [TestMethod]
        public void TestMethodDecrypt6()
        {
            vw.Init(0, "lemon");
            Assert.AreEqual(vw.Decrypt("l!n!p!h!"), "a!b!c!d!");
        }

        [TestMethod]
        public void TestMethodDecrypt7()
        {
            vw.Init(7, "a");
            Assert.AreEqual(vw.Decrypt("!#$%567:78*^.,/'}{[]<>()"), "!#$%567:78*^.,/'}{[]<>()");
        }

        [TestMethod]
        public void TestMethodDecrypt8()
        {
            vw.Init(0, "lemon");
            Assert.AreEqual(vw.Decrypt("lforrq"), "abcdef");
        }

        [TestMethod]
        public void TestMethodDecrypt9()
        {
            vw.Init(12, "z");
            Assert.AreEqual(vw.Decrypt("WXYGH"), "XYZHI");
        }

        [TestMethod]
        public void TestMethodDecrypt10()
        {
            vw.Init(0, "a");
            Assert.AreEqual(vw.Decrypt(""), "");
        }

    }
}
