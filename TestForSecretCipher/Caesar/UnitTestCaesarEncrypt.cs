﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Encryption_Module;

namespace TestForSecretCipher.Caesar
{
    [TestClass]
    public class UnitTestCaesarEncrypt
    {
        Caesar_Worker cw = new Caesar_Worker();

        [TestMethod]
        public void TestMethodEncrypt1()
        {
            cw.Init(0);
            Assert.AreEqual(cw.Encrypt("abcD"), "abcD");
        }

        [TestMethod]
        public void TestMethodEncrypt2()
        {
            cw.Init(-1);
            Assert.AreEqual(cw.Encrypt("Abcd"), "Zabc");
        }

        [TestMethod]
        public void TestMethodEncrypt3()
        {
            cw.Init(26);
            Assert.AreEqual(cw.Encrypt("abXy"), "abXy");
        }

        [TestMethod]
        public void TestMethodEncrypt4()
        {
            cw.Init(-26);
            Assert.AreEqual(cw.Encrypt("a B c d  "), "a B c d  ");
        }

        [TestMethod]
        public void TestMethodEncrypt5()
        {
            cw.Init(100);
            Assert.AreEqual(cw.Encrypt("AbCd"), "WxYz");
        }

        [TestMethod]
        public void TestMethodEncrypt6()
        {
            cw.Init(-78);
            Assert.AreEqual(cw.Encrypt("A!BCD"), "A!BCD");
        }

        [TestMethod]
        public void TestMethodEncrypt7()
        {
            cw.Init(60000000);
            Assert.AreEqual(cw.Encrypt("ABcD"), "IJkL");
        }

        [TestMethod]
        public void TestMethodEncrypt8()
        {
            cw.Init(180);
            Assert.AreEqual(cw.Encrypt(",.<>!#№546эфыв!-+=&^$*~{}[]"), ",.<>!#№546эфыв!-+=&^$*~{}[]");
        }

        [TestMethod]
        public void TestMethodEncrypt9()
        {
            cw.Init(-31);
            Assert.AreEqual(cw.Encrypt("Zxy#.,-xFw()"), "Ust#.,-sAr()");
        }

        [TestMethod]
        public void TestMethodEncrypt10()
        {
            cw.Init(-4000);
            Assert.AreEqual(cw.Encrypt(""), "");
        }


    }
}
