﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Encryption_Module;

namespace TestForSecretCipher.Caesar
{
    [TestClass]
    public class UnitTestCaesarInit
    {
        Caesar_Worker cw = new Caesar_Worker();

        [TestMethod]
        public void TestMethodInit1()
        {
            cw.Init(0);
            Assert.AreEqual(cw.shift, 0);
        }

        [TestMethod]
        public void TestMethodInit2()
        {
            cw.Init(-5);
            Assert.AreEqual(cw.shift, 21);
        }

        [TestMethod]
        public void TestMethodInit3()
        {
            cw.Init(100000);
            Assert.AreEqual(cw.shift, 4);
        }


    }
}
