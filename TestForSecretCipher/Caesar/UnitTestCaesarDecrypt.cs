﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Encryption_Module;

namespace TestForSecretCipher.Caesar
{
    [TestClass]
    public class UnitTestCaesarDecrypt
    {
        Caesar_Worker cw = new Caesar_Worker();

        [TestMethod]
        public void TestMethodDecrypt1()
        {
            cw.Init(0);
            Assert.AreEqual(cw.Decrypt("abcD"), "abcD");
        }

        [TestMethod]
        public void TestMethodDecrypt2()
        {
            cw.Init(-27);
            Assert.AreEqual(cw.Decrypt("Abcd"), "Bcde");
        }

        [TestMethod]
        public void TestMethodDecrypt3()
        {
            cw.Init(26);
            Assert.AreEqual(cw.Decrypt("abXy"), "abXy");
        }

        [TestMethod]
        public void TestMethodDecrypt4()
        {
            cw.Init(-25);
            Assert.AreEqual(cw.Decrypt("a B c d  "), "z A b c  ");
        }

        [TestMethod]
        public void TestMethodDecrypt5()
        {
            cw.Init(100);
            Assert.AreEqual(cw.Decrypt("AbCdaaaaaaaaaaaaaaaaaaaaA"),
                "EfGheeeeeeeeeeeeeeeeeeeeE");
        }

        [TestMethod]
        public void TestMethodDecrypt6()
        {
            cw.Init(-78);
            Assert.AreEqual(cw.Decrypt("A!BCDac"), "A!BCDac");
        }

        [TestMethod]
        public void TestMethodDecrypt7()
        {
            cw.Init(60000000);
            Assert.AreEqual(cw.Decrypt("ABcD"), "STuV");
        }

        [TestMethod]
        public void TestMethodDecrypt8()
        {
            cw.Init(180);
            Assert.AreEqual(cw.Decrypt(",.<>!#№546эфыв!-+=&^$*~{}[]"), ",.<>!#№546эфыв!-+=&^$*~{}[]");
        }

        [TestMethod]
        public void TestMethodDecrypt9()
        {
            cw.Init(-31);
            Assert.AreEqual(cw.Decrypt("Zxy#.,-xFw()"), "Ecd#.,-cKb()");
        }

        [TestMethod]
        public void TestMethodDecrypt10()
        {
            cw.Init(-4000);
            Assert.AreEqual(cw.Decrypt(""), "");
        }


    }
}
