﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Encryption_Module;


namespace TestForSecretCipher.Affine
{
    [TestClass]
    public class UnitTestAffineEncrypt
    {
        Affine_Worker aw = new Affine_Worker();

        [TestMethod]
        public void TestMethodEncrypt1()
        {
            aw.Init(0);
            Assert.AreEqual(aw.Encrypt("abcd"), "azyx");
        }

        [TestMethod]
        public void TestMethodEncrypt2()
        {
            aw.Init(10);
            Assert.AreEqual(aw.Encrypt("AbcD"), "KjiH");
        }

        [TestMethod]
        public void TestMethodEncrypt3()
        {
            aw.Init(25);
            Assert.AreEqual(aw.Encrypt("XYZA"), "CBAZ");
        }

        [TestMethod]
        public void TestMethodEncrypt4()
        {
            aw.Init(-4);
            Assert.AreEqual(aw.Encrypt("lemon lemon"), "lskij lskij");
        }

        [TestMethod]
        public void TestMethodEncrypt5()
        {
            aw.Init(0);
            Assert.AreEqual(aw.Encrypt("A!B!C!D!"), "A!Z!Y!X!");
        }

        [TestMethod]
        public void TestMethodEncrypt6()
        {
            aw.Init(-125);
            //Assert.AreEqual(aw.Encrypt("abcdefgxyz"), "fedcbazihg");
            string tmp = (aw.Encrypt("a"));
            Assert.AreEqual(aw.Encrypt("a"), "f");
        }

        [TestMethod]
        public void TestMethodEncrypt7()
        {
            aw.Init(1000);
            Assert.AreEqual(aw.Encrypt("Z!@#45%^;'&*()[]15ваптб"), "N!@#45%^;'&*()[]15ваптб");
        }

        [TestMethod]
        public void TestMethodEncrypt8()
        {
            aw.Init(100);
            Assert.AreEqual(aw.Encrypt("A"), "W");
        }

        [TestMethod]
        public void TestMethodEncrypt9()
        {
            aw.Init(-100);
            Assert.AreEqual(aw.Encrypt("Zx"), "Fh");
        }

        [TestMethod]
        public void TestMethodEncrypt10()
        {
            aw.Init(100);
            Assert.AreEqual(aw.Encrypt(""), "");
        }
    }
}
