﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Encryption_Module;


namespace TestForSecretCipher.Affine
{
    [TestClass]
    public class UnitTestAffineDecrypt
    {
        Affine_Worker aw = new Affine_Worker();

        [TestMethod]
        public void TestMethodDecrypt1()
        {
            aw.Init(0);
            Assert.AreEqual(aw.Encrypt("azyx"), "abcd");
        }

        [TestMethod]
        public void TestMethodDecrypt2()
        {
            aw.Init(10);
            Assert.AreEqual(aw.Encrypt("KjiH"), "AbcD");
        }

        [TestMethod]
        public void TestMethodDecrypt3()
        {
            aw.Init(25);
            Assert.AreEqual(aw.Encrypt("CBAZ"), "XYZA");
        }

        [TestMethod]
        public void TestMethodDecrypt4()
        {
            aw.Init(-4);
            Assert.AreEqual(aw.Encrypt("lskij lskij"), "lemon lemon");
        }

        [TestMethod]
        public void TestMethodDecrypt5()
        {
            aw.Init(0);
            Assert.AreEqual(aw.Encrypt("A!Z!Y!X!"), "A!B!C!D!");
        }

        [TestMethod]
        public void TestMethodDecrypt6()
        {
            aw.Init(-125);
            //Assert.AreEqual(aw.Encrypt("abcdefgxyz"), "fedcbazihg");
            Assert.AreEqual(aw.Encrypt("a"), "f");
        }

        [TestMethod]
        public void TestMethodDecrypt7()
        {
            aw.Init(1000);
            Assert.AreEqual(aw.Encrypt("N!@#45%^;'&*()[]15ваптб"), "Z!@#45%^;'&*()[]15ваптб");
        }

        [TestMethod]
        public void TestMethodDecrypt8()
        {
            aw.Init(100);
            Assert.AreEqual(aw.Encrypt("W"), "A");
        }

        [TestMethod]
        public void TestMethodDecrypt9()
        {
            aw.Init(-100);
            Assert.AreEqual(aw.Encrypt("Fh"), "Zx");
        }

        [TestMethod]
        public void TestMethodDecrypt10()
        {
            aw.Init(100);
            Assert.AreEqual(aw.Encrypt(""), "");
        }
    }
}
