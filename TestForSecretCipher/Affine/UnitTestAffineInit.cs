﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Encryption_Module;


namespace TestForSecretCipher.Affine
{
    [TestClass]
    public class UnitTestAffineInit
    {
        Affine_Worker aw = new Affine_Worker();

        [TestMethod]
        public void TestMethodInit1()
        {
            aw.Init(1);
            Assert.AreEqual(aw.b, 1);
        }

        [TestMethod]
        public void TestMethodInit2()
        {
            aw.Init(-90);
            Assert.AreEqual(aw.b, -90);
        }

        [TestMethod]
        public void TestMethodInit3()
        {
            aw.Init(0);
            Assert.AreEqual(aw.b, 0);
        }
    }
}
