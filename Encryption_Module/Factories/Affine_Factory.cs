﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encryption_Module.Factories
{
    /// <summary>
    /// Фабрика Афинщиков
    /// </summary>
    public class Affine_Factory : AbstractFactory
    {
        public override AbstractWorker MakeWorker()
        {
            Affine_Worker worker = new Affine_Worker();
            return worker;
        }
    }
}
