﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encryption_Module.Factories
{
    /// <summary>
    /// Фабрика Виженерщиков
    /// </summary>
    public class Vigenere_Factory : AbstractFactory
    {
        public override AbstractWorker MakeWorker()
        {
            Vigenere_Worker worker = new Vigenere_Worker();
            return worker;
        }
    }
}
