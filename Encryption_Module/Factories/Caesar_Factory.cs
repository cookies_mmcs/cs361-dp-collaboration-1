﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encryption_Module.Factories
{
    /// <summary>
    /// Фабрика Цезарёвщиков
    /// </summary>
    public class Caesar_Factory : AbstractFactory
    {
        public override AbstractWorker MakeWorker()
        {
            Caesar_Worker worker = new Caesar_Worker();
            return worker;
        }
    }
}
