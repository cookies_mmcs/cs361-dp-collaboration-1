﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encryption_Module.Factories
{
    /// <summary>
    /// Абстрактная фабрика рабочих 
    /// </summary>
    public abstract class AbstractFactory
    {
        /// <summary>
        /// Генерация рабочего
        /// </summary>
        /// <param name="shift"></param>
        /// <returns></returns>
        public abstract AbstractWorker MakeWorker();
    }
}
