﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encryption_Module
{
    /// <summary>
    /// Необходимые константы
    /// </summary>
    static public class Consants
    {
        public const int power = 26;
        public const int power_n = 10;
        public const int a = 25;
    }
}
