﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Encryption_Module.Factories;

namespace Encryption_Module
{
    /// <summary>
    /// Our big wheel
    /// </summary>
    public class Superintendent
    {
        public bool crypt_direction;
        public string text;
        public int workers;
        public string key;
        public int cipher;

        /// <summary>
        /// Superintendent initialization
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="t"></param>
        /// <param name="w"></param>
        /// <param name="k"></param>
        /// <param name="c"></param>
        public void Init(bool dir, string t, int w, string k, int c)
        {
            crypt_direction = dir;

            text = t;

            if (w > 0)
                workers = w;
            else
                throw new ArgumentException("Non-positive amount of workers.");


            int z = 0;
            switch (c)
            {
                case 0:
                case 2:
                    if (Int32.TryParse(k, out z))
                        key = k;
                    else
                        throw new ArgumentException("Must be number");
                    break;
                case 1:
                    bool flag = false;
                    foreach (char ch in k)
                        if (!((ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z')))
                            flag = true;
                    if (!flag)
                        key = k;
                    else
                        throw new ArgumentException("Invalid symbols in key");
                    break;
                default:
                    throw new ArgumentException("Invalid cipher");
            }

            cipher = c;

        }

        /// <summary>
        /// Распределение символов по рабочим
        /// </summary>
        /// <returns></returns>
        public int[] SpreadingWork()
        {
            int[] spreading = new int[workers];
            int work = text.Length / workers;
            int other_work = text.Length % workers;
            for (int i = 0; i < spreading.Length; ++i)
                spreading[i] = work;
            for (int i = 0; i < other_work; ++i)
                ++spreading[i];
            return spreading;
        }

        /// <summary>
        /// Создание штата рабочих
        /// </summary>
        /// <param name="factory">ggg</param>
        /// <param name="work_shedule"></param>
        /// <returns></returns>
        public List<AbstractWorker> MakeStaff(AbstractFactory factory, int[] work_shedule)
        {
            List<AbstractWorker> staff = new List<AbstractWorker>();
            switch (cipher)
            {
                case 0:
                    for (int i = 0; i < workers; ++i)
                    {
                        staff.Add(new Caesar_Worker());
                        (staff[i] as Caesar_Worker).Init(Convert.ToInt32(key));
                    }
                    break;
                case 1:
                    int j = 0;
                    for (int i = 0; i < workers; ++i)
                    {
                        staff.Add(new Vigenere_Worker());
                        (staff[i] as Vigenere_Worker).Init(j, key);
                        j += work_shedule[i];
                    }
                    break;
                case 2:
                    for (int i = 0; i < workers; ++i)
                    {
                        staff.Add(new Affine_Worker());
                        (staff[i] as Affine_Worker).Init(Convert.ToInt32(key));
                    }
                    break;
                default:
                    throw new ArgumentException("Wrong cipher");

            }
            return staff;
        }

        /// <summary>
        /// Получение нужного результата
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="work_shedule"></param>
        /// <returns></returns>
        public string ResultText(List<AbstractWorker> staff, int[] work_shedule)
        {
            string result = "";
            if (crypt_direction)
            {
                int i = 0;
                int pos = 0;
                foreach (AbstractWorker worker in staff)
                {
                    result += worker.Encrypt(text.Substring(pos, work_shedule[i]));
                    pos += work_shedule[i++];
                }
            }
            else
            {
                int i = 0;
                int pos = 0;
                foreach (AbstractWorker worker in staff)
                {
                    result += worker.Decrypt(text.Substring(pos, work_shedule[i]));
                    pos += work_shedule[i++];
                }
            }
            return result;
        }

        /// <summary>
        /// Функция работы с шифром
        /// </summary>
        /// <returns></returns>
        public string WorkWithCipher()
        {
            int[] work_shedule = SpreadingWork();
            AbstractFactory factory;
            switch (cipher)
            {
                case 0:
                    factory = new Caesar_Factory();
                    break;
                case 1:
                    factory = new Vigenere_Factory();
                    break;
                case 2:
                    factory = new Affine_Factory();
                    break;
                default:
                    throw new ArgumentException("Wrong cipher");

            }
            List<AbstractWorker> staff = MakeStaff(factory, work_shedule);
            return ResultText(staff, work_shedule);
        }
    }
}
