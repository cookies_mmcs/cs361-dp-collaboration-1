﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encryption_Module
{
    /// <summary>
    /// Абстрактный рабочий
    /// </summary>
    public abstract class AbstractWorker
    {
        public bool IsLatinLetter(char c)
        {
            return c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z';
        }
        /// <summary>
        /// Шифровка текста
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public abstract string Encrypt(string text);

        /// <summary>
        /// Дешифровка текста
        /// </summary>
        /// <param name="cipher"></param>
        /// <returns></returns>
        public abstract string Decrypt(string cipher);
    }
}
