﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encryption_Module
{
    /// <summary>
    /// Рабочий для шифра Виженера
    /// </summary>
    public class Vigenere_Worker : AbstractWorker
    {
        public int pos;
        public List<int> key_shift = new List<int>();

        /// <summary>
        /// Инициализация ключом и значением сдвига ключа относительно первого символа
        /// </summary>
        /// <param name="position"></param>
        /// <param name="key"></param>
        public void Init(int position, string key)
        {
            if (position < 0 || key == "")
                throw new ArgumentException("Position must be greater than 0");
            pos = position;
            int x = pos % key.Length;
            key = key.Substring(x, key.Length - x) + key.Substring(0, x);
            foreach (char c in key)
            {
                if (!char.IsLetter(c))
                    throw new ArgumentException("Invalid key");
                if (char.IsUpper(c))
                    key_shift.Add(c - 'A');
                else key_shift.Add(c - 'a');
            }
        }

        /// <summary>
        /// Функция зашифровки
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public override string Encrypt(string text)
        {
            string res = "";
            for (int i = 0; i < text.Length; ++i)
            {
                if (!IsLatinLetter(text[i]))
                {
                    res += text[i];
                    continue;
                }
                int shift = 'a';
                if (char.IsUpper(text[i]))
                    shift = 'A';
                res += Convert.ToChar(shift + (key_shift[(i % key_shift.Count)] + text[i] - shift) % Consants.power);
            }
            return res;
        }

        /// <summary>
        /// Функция расшифровки
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public override string Decrypt(string text)
        {
            string res = "";
            for (int i = 0; i < text.Length; ++i)
            {
                if (!IsLatinLetter(text[i]))
                {
                    res += text[i];
                    continue;
                }
                int shift = 'a';
                if (char.IsUpper(text[i]))
                    shift = 'A';
                res += Convert.ToChar(shift + (text[i] - key_shift[(i % key_shift.Count)] + Consants.power - shift) % Consants.power);
            }
            return res;
        }


    }

}
