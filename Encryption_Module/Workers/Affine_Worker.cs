﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encryption_Module
{
    /// <summary>
    /// Рабочий для шифра Афинного шифра
    /// </summary>
    public class Affine_Worker : AbstractWorker
    {
        public int b;

        /// <summary>
        /// Инициализация ключа
        /// </summary>
        /// <param name="key"></param>
        public void Init(int key)
        {
            b = key;
        }

        /// <summary>
        /// Внутренняя функция для вычисления зашифрованной буквы
        /// </summary>
        /// <param name="input"></param>
        /// <param name="sign"></param>
        /// <returns></returns>
        public char ComputeChar(char input, int dir)
        {
            int register = Convert.ToInt32(Char.IsLower(input));
            register = (65 + register * 32);
            if (dir == 1)
            {
                char tmp = Convert.ToChar((((input - register) * Consants.a + b) % Consants.power + Consants.power) % Consants.power + register);
                return tmp;
            }
            else if (dir == 0)
            {
                char tmp = Convert.ToChar(((-1) * ((input - register) - b) % Consants.power + Consants.power) % Consants.power + register);
                return tmp;
            }
            else
                throw new ArgumentException("Argument must be 0 or 1");
        }

        /// <summary>
        /// Функция зашифровки
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public override string Encrypt(string text)
        {
            string result = "";
            foreach (char c in text)
            {
                if (IsLatinLetter(c))
                    result += ComputeChar(c, 1);
                else
                    result += c;
            }
            return result;
        }

        /// <summary>
        /// Функция расшифровки
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public override string Decrypt(string cipher)
        {
            string result = "";
            foreach (char c in cipher)
            {
                if (IsLatinLetter(c))
                    result += ComputeChar(c, 0);
                else
                    result += c;
            }
            return result;
        }
    }
}
