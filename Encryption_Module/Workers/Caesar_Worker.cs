﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encryption_Module
{
    /// <summary>
    /// Рабочий для шифра Цезаря
    /// </summary>
    public class Caesar_Worker : AbstractWorker
    {
        public int shift;

        /// <summary>
        /// Инициализация значением сдвига
        /// </summary>
        /// <param name="sh"></param>
        public void Init(int sh)
        {
            shift = (sh % Consants.power + Consants.power) % Consants.power;
        }

        /// <summary>
        /// Внутренняя функция для вычисления зашифрованной буквы
        /// </summary>
        /// <param name="input"></param>
        /// <param name="sign"></param>
        /// <returns></returns>
        public char ComputeChar(char input, int sign)
        {
            int register = Convert.ToInt32(Char.IsLower(input));
            register = (65 + register * 32);
            return Convert.ToChar((input + sign * shift - register + Consants.power * (1 - sign)) % Consants.power + register);
        }

        /// <summary>
        /// Функция шифровки
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public override string Encrypt(string text)
        {
            string result = "";
            foreach (char c in text)
            {
                if (IsLatinLetter(c))
                    result += ComputeChar(c, 1);
                else
                    result += c;
            }
            return result;
        }

        /// <summary>
        /// Функция дешифровки
        /// </summary>
        /// <param name="cipher"></param>
        /// <returns></returns>
        public override string Decrypt(string cipher)
        {
            string result = "";
            foreach (char c in cipher)
            {
                if (IsLatinLetter(c))
                    result += ComputeChar(c, -1);
                else
                    result += c;
            }
            return result;
        }

    }
}
